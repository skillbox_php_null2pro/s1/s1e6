<?
require_once 'core/router.php'; //код роутера
require_once 'template/menu/menu.php'; //регистрируем компонент меню
require_once 'core/helpers.php';
require_once 'core/Session.php';
require_once 'core/Cookie.php';
require_once 'logins.php';
require_once 'passwords.php';

$session = new Session('session_id', 20 * 60, 'sha256');
$cookie = new Cookie('login', 50 * 24 * 3600);

$login = $cookie->getValue();
$isAuthorized = $session->isAuth();
$isAuthorizable = !empty($_POST['login']) && !empty($_POST['password']); //Поля авторизации заполнены?
$isShowAuthForm = !$isAuthorized && isset($_GET['login']); //Показываем форму авторизации?
$isFail = false; //Показываем ошибку?

if (isset($_GET['logout'])) {
    $session->destroy();
    go();
}

//Авторизация
if (!$isAuthorized && $isAuthorizable) {
    $login = htmlspecialchars(trim($_POST['login']));
    $password = htmlspecialchars(trim($_POST['password']));
    $cookie->setValue($login);
    $isAuthorized = $password === $arPasswords[array_search($login, $arLogins, true)];
    $isFail = !$isAuthorized;

    if ($isAuthorized) {
        $session->auth();
        go();
    }
}

//Проверка активности
if ($isAuthorized) {
    $session->refresh();
    $cookie->refresh();
} elseif ((parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH) != '/') || !isset($_GET['login'])) {
    $session->destroy();
    go('/?login');
}


//Передаем данные для автозаполнения
?>
<script>
    var defLogin = '<?=reset($arLogins)?>';
    var defPassword = '<?=reset($arPasswords)?>';
</script>
