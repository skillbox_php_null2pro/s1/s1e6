<?php


class Cookie
{
    private $name;
    private $lifeTime;

    public function __construct($name, $lifeTime)
    {
        $this->name = $name;
        $this->lifeTime = $lifeTime;
    }

    public function refresh()
    {
        if (!$this->getValue()) {
            return false;
        }

        return $this->setValue($this->getValue());
    }

    public function setValue($value)
    {
        return setcookie($this->name, $value, time() + $this->lifeTime, '/');
    }

    public function getValue()
    {
        return isset($_COOKIE[$this->name]) ? htmlspecialchars(trim($_COOKIE[$this->name])) : null;
    }
}