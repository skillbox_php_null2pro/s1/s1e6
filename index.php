<? require_once 'include/template/header.php' ?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="left-collum-index">

            <h1>Возможности проекта —</h1>
            <p>Вести свои личные списки, например покупки в магазине, цели, задачи, и многое другое.
                Делиться списками с друзьями, и просматривать списки друзей.</p>


        </td>
        <td class="right-collum-index">
            <? if ($isShowAuthForm && !$isAuthorized): ?>
                <div class="project-folders-menu">
                    <ul class="project-folders-v">
                        <li class="project-folders-v-active"><span>Авторизация</span></li>
                        <li><a href="#">Регистрация</a></li>
                        <li><a href="#">Забыли пароль?</a></li>
                    </ul>
                    <div style="clear: both;"></div>
                </div>
                <? if ($isFail): ?>
                    <?= include 'include/fail.php' ?>
                <? endif ?>
                <form method="post" action="?login" name="auth">
                    <div class="index-auth">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <?if ($cookie->getValue()):?>
                            <tr hidden>
                            <?else:?>
                            <tr>
                            <?endif;?>
                                <td class="iat">Ваш логин:
                                    <br />
                                    <input id="login_id" size="30" name="login" value="<?= $login ?? '' ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="iat">Ваш пароль:
                                    <br />
                                    <input id="password_id" size="30" name="password" value="<?= $password ?? '' ?>"
                                      type="password"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="submit" value="Войти" />
                                    <input id="test" type="button" value="Тест ввода" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </form>
            <? endif ?>
            <? if ($isAuthorized): ?>
                <? require_once 'include/success.php' ?>
            <? endif ?>
        </td>
    </tr>
</table>

<? require_once 'include/template/footer.php';
